package com.example.petfriend.model

data class Animal(var nom:String, var rac: String, var loc:String){
    val nome: String = nom
    val raca: String = rac
    val localizacao: String = loc
}
