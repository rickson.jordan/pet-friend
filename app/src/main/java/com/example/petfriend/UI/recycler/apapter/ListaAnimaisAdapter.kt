package com.example.petfriend.UI.recycler.apapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.petfriend.databinding.AnimaisItemBinding
import com.example.petfriend.model.Animal

class ListaAnimaisAdapter(
    private val context: Context,
    animais: List<Animal>) :
    RecyclerView.Adapter<ListaAnimaisAdapter.ViewHolder>()
{
    private val animais = animais.toMutableList()

    class ViewHolder(private val binding: AnimaisItemBinding) :
    RecyclerView.ViewHolder(binding.root){

        fun vincula(animal: Animal) {
            val nome = binding.animalItemNome
            nome.text = animal.nome
            val raca = binding.animalItemRaca
            raca.text = animal.raca
            val loc = binding.animalItemLocalizacao
            loc.text = animal.localizacao
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        //pega cada view e faz o processo de buid (ele que pega e faz as modificações)
        val inflater = LayoutInflater.from(context)
        val binding = AnimaisItemBinding.inflate(inflater,parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //qual é o item que estamos, posição e o view holder
        val animal = animais[position]
        holder.vincula(animal)
    }

    override fun getItemCount(): Int {
        // determina para o adapter quantos itens a gente quer apresentar dentro dele
        return animais.size
    }

    fun atualiza(animais: List<Animal>) {
        this.animais.clear()
        this.animais.addAll(animais)
        notifyDataSetChanged()
    }

}
