package com.example.petfriend.UI.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.petfriend.R
import com.example.petfriend.UI.recycler.apapter.ListaAnimaisAdapter
import com.example.petfriend.dao.AnimaisDao
import com.example.petfriend.databinding.ActivityFormularioAnimalBinding
import com.example.petfriend.databinding.ActivityListaAnimaisBinding
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ListaAnimaisActivity : AppCompatActivity(R.layout.activity_lista_animais)  {
    // Data Access Object - DAO
    private val dao = AnimaisDao()
    private val adapter = ListaAnimaisAdapter(context = this, animais = dao.buscaTodos())
    private val binding by lazy {
        ActivityListaAnimaisBinding.inflate(layoutInflater)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        configuraRecyclerView()
        configuraFab()
    }

    override fun onResume() {
        super.onResume()
        adapter.atualiza(dao.buscaTodos())
    }

    private fun configuraFab() {
        val fab = binding.activityListaAnimaisFAB
        fab.setOnClickListener() {
            vaiParaFormularioAnimais()
        }
    }

    private fun vaiParaFormularioAnimais() {
        val intent = Intent(this, FormularioAnimalActivity::class.java)
        startActivity(intent)
    }

    private fun configuraRecyclerView() {
        val recyclerview = binding.activityListaAnimaisRecyclerView
        recyclerview.adapter = adapter
    }
}