package com.example.petfriend.UI.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.petfriend.R
import com.example.petfriend.dao.AnimaisDao
import com.example.petfriend.databinding.ActivityFormularioAnimalBinding
import com.example.petfriend.model.Animal

class FormularioAnimalActivity : AppCompatActivity(R.layout.activity_formulario_animal) {
    private val binding by lazy {
        ActivityFormularioAnimalBinding.inflate(layoutInflater)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        configuraBotaoSalvar()
    }

    private fun configuraBotaoSalvar() {
        val botaoSalvar = binding.activityFormularioBotaoSalvar
        val dao = AnimaisDao()
        botaoSalvar.setOnClickListener {
            val novoAnimal = criaAnimal()
            //Log.i("FormularioAnimal", "onCreate: $novoAnimal")
            dao.adiciona(novoAnimal)
            //Log.i("FormularioAnimal", "onCreate: ${dao.buscaTodos()}")
            finish()
        }
    }

    private fun criaAnimal(): Animal {
        val campoNome = binding.activityFormularioItemNome
        val nome = campoNome.text.toString()
        val campoRaca = binding.activityFormularioItemRaca
        val raca = campoRaca.text.toString()
        val campoLocalizacao = binding.activityFormularioItemLocalizacao
        val localizacao = campoLocalizacao.text.toString()

        return Animal(
            nom = nome,
            rac = raca,
            loc = localizacao
        )
    }
}