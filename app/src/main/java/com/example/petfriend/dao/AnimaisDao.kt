package com.example.petfriend.dao

import com.example.petfriend.model.Animal

class AnimaisDao {

    companion object {
        private val animais = mutableListOf<Animal>()
    }

    fun adiciona(animal : Animal){
        Companion.animais.add(animal)
    }
    fun buscaTodos(): List<Animal>{
        return Companion.animais.toList()
    }


}